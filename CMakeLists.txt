cmake_minimum_required(VERSION 3.10)

find_package(PkgConfig REQUIRED)
pkg_check_modules(SFML REQUIRED sfml-all)

add_executable( 
    testapp 
    src/main.cpp
    src/game.cpp
    src/panels/intropanel.cpp
    src/panels/playpanel.cpp
    src/sprites/crosscursor.cpp
    src/sprites/character.cpp
    src/sprites/bullet.cpp
    src/sprites/bulletsmanager.cpp
)

OPTION(DEBUG "Enable development mode" ON)

IF(DEBUG)
    ADD_DEFINITIONS(-DDATA_SRC)
ENDIF(DEBUG)

target_link_libraries(testapp ${SFML_LIBRARIES} ${SFML_DEPENDENCIES})
target_include_directories(testapp PUBLIC ${SFML_INCLUDE_DIR})
target_compile_options(testapp PUBLIC ${SFML_CFLAGS_OTHER})
