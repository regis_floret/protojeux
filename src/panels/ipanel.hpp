/*
 * Interface for all panel
 */

#ifndef __IPANEL_H__
#define __IPANEL_H__

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class IPanel
{
public:
  enum Action
  {
    Nothing = -1,
    Quit,
    Play
  };

  virtual Action move(sf::Event &event) = 0;
  virtual void render(sf::RenderWindow &window) = 0;
};

#endif
