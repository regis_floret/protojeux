#include "intropanel.hpp"

#include <iostream>

IntroPanel::IntroPanel()
{
    font.loadFromFile("data/Game-Over.ttf");

    createTextSprite(playText, "PLAY", 0);
    createTextSprite(quitText, "QUIT", playText.getLocalBounds().height + 100);
}

void IntroPanel::createTextSprite(sf::Text &textSprite, const std::string &text, const float top)
{
    textSprite.setFont(font);
    textSprite.setString(text);
    textSprite.setFillColor(sf::Color::White);
    textSprite.setCharacterSize(200);

    sf::Vector2f position((1024 - textSprite.getLocalBounds().width) / 2, top);

    textSprite.setPosition(position);
}

IPanel::Action IntroPanel::move(sf::Event &event)
{
    IPanel::Action action = IPanel::Action::Nothing;

    if (event.type == sf::Event::MouseMoved)
    {
        onMouseMoveEvent(event);
    }

    else if (event.type == sf::Event::MouseButtonReleased)
    {
        action = onMouseClickEvent(event);
    }

    return action;
}

void IntroPanel::render(sf::RenderWindow &window)
{
    window.draw(playText);
    window.draw(quitText);
    window.draw(cursor);
}

void IntroPanel::onMouseMoveEvent(sf::Event &event)
{
    cursor.moveTo(event.mouseMove.x, event.mouseMove.y);

    sf::Vector2f mousePosition(event.mouseMove.x, event.mouseMove.y);

    sf::Color playColour = sf::Color::White;
    sf::Color quitColour = sf::Color::White;

    if (playText.getGlobalBounds().contains(mousePosition))
    {
        playColour = sf::Color::Red;
    }

    if (quitText.getGlobalBounds().contains(mousePosition))
    {
        quitColour = sf::Color::Red;
    }

    playText.setFillColor(playColour);
    quitText.setFillColor(quitColour);
}

IPanel::Action IntroPanel::onMouseClickEvent(sf::Event &event)
{
    IPanel::Action action = IPanel::Action::Nothing;

    sf::Vector2f mousePosition(event.mouseButton.x, event.mouseButton.y);

    if (playText.getGlobalBounds().contains(mousePosition))
    {
        action = IPanel::Action::Play;
    }

    if (quitText.getGlobalBounds().contains(mousePosition))
    {
        action = IPanel::Action::Quit;
    }

    return action;
}