#ifndef __INTRO_PANEL_H__
#define __INTRO_PANEL_H__

#include "ipanel.hpp"
#include "../sprites/crosscursor.hpp"

class IntroPanel : public IPanel
{
public: 
    IntroPanel();
    Action move(sf::Event & evt);
    void render(sf::RenderWindow & window);

private:
    void onMouseMoveEvent(sf::Event & event);
    Action onMouseClickEvent(sf::Event & event);

    void createTextSprite(sf::Text & textSprite, const std::string & text, const float top);

private:
    sf::Font font;
    sf::Text playText;
    sf::Text quitText;

    CrossCursorSprite cursor;
};

#endif
