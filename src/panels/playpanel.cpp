#include "playpanel.hpp"
#include "../constants.hpp"

#include <math.h>
#include <iostream>

PlayPanel::PlayPanel()
{
    cursor.setPosition(100, 100);
}

IPanel::Action PlayPanel::move(sf::Event &event)
{
    float angle = 0.0;

    switch (event.type)
    {
    case sf::Event::MouseMoved:
        sprite.rotateFromMousePosition(event.mouseMove.x, event.mouseMove.y);
        cursor.moveTo(event.mouseMove.x, event.mouseMove.y);
        break;

    case sf::Event::MouseButtonPressed:
        bullet.setPosition(sf::Vector2f(event.mouseButton.x, event.mouseButton.y));
        bullet.setVisible(true); 
        break;

    case sf::Event::KeyPressed:
        handleKeyboard(event);
        break;

    default:;
    }

    return IPanel::Action::Nothing;
}

void PlayPanel::render(sf::RenderWindow &window)
{
    window.draw(sprite);
    window.draw(cursor);
    if (bullet.isVisible()) 
    {
        window.draw(bullet);
    }
}

void PlayPanel::handleKeyboard(sf::Event &evt)
{
    CharacterSprite::Movment mvt;

    if (evt.key.code == sf::Keyboard::Q)
    {
        mvt = CharacterSprite::Movment::Left;
    }

    if (evt.key.code == sf::Keyboard::D)
    {
        mvt = CharacterSprite::Movment::Right;
    }

    if (evt.key.code == sf::Keyboard::Z)
    {
        mvt = CharacterSprite::Movment::Up;
    }

    if (evt.key.code == sf::Keyboard::S)
    {
        mvt = CharacterSprite::Movment::Down;
    }

    sprite.move(mvt);
}
