#ifndef __PLAY_PANEL_H__
#define __PLAY_PANEL_H__

#include "ipanel.hpp"
#include "../sprites/crosscursor.hpp"
#include "../sprites/character.hpp"
#include "../sprites/bullet.hpp"

#include <SFML/Graphics.hpp>

class PlayPanel : public IPanel
{
public:
  PlayPanel();

  IPanel::Action move(sf::Event &event);
  void render(sf::RenderWindow &window);

private:
  void handleKeyboard(sf::Event & evt);
  float getAngleFromMouse(const int pos_x, const int pos_y) const;

private:
  CrossCursorSprite cursor;
  CharacterSprite sprite;
  BulletSprite bullet;
};

#endif
