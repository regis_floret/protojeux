#include "crosscursor.hpp"


CrossCursorSprite::CrossCursorSprite()
{
    texture.loadFromFile("data/crosscursor.png");
    setTexture(texture);
}

void CrossCursorSprite::moveTo(const int x, const int y)
{
    setPosition(sf::Vector2f(x, y));
}

