#ifndef __CROSS_CURSOR_H__
#define __CROSS_CURSOR_H__

#include <SFML/Graphics.hpp>

class CrossCursorSprite : public sf::Sprite
{
public:
  CrossCursorSprite();
  void moveTo(const int x, const int y);

private:
  sf::Texture texture;
};

#endif