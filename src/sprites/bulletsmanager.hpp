#ifndef __BULLETS_MANAGER_HPP__
#define __BULLETS_MANAGER_HPP__

#include "bullet.hpp"

#include <list>
#include <SFML/System.hpp>

class BulletsManager
{
  public:
    BulletsManager();
    void addBullet(const sf::Vector2f from, const sf::Vector2f to);
    bool canAddBullet() const;

  private:
    std::list<BulletSprite*> bullets;
    sf::Clock lastShoot;
    float rotation;
};

#endif
