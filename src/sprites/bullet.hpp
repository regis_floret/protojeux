#ifndef __BULLET_HPP__
#define __BULLET_HPP__

#include <SFML/Graphics.hpp>

class BulletSprite : public sf::Sprite
{
  public:
    BulletSprite();

    void moveTo(float x, float y);
    bool isVisible() const;
    void setVisible(const bool);
    
  private:
    sf::Texture texture;
    bool visible;
};

#endif