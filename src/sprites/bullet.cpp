#include "bullet.hpp"

BulletSprite::BulletSprite() : visible(false)
{
    texture.loadFromFile("data/bullet.png");
    setTexture(texture);
}

void BulletSprite::moveTo(const float x, const float y)
{
    setPosition(x,y);
}

 bool BulletSprite::isVisible() const
 {
     return this->visible;
 }

void BulletSprite::setVisible(const bool visible)
{
    this->visible = visible;
}
