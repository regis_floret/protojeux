#include "bulletsmanager.hpp"

static const sf::Int32 BeetweenTwoShootInMs = 50;

BulletsManager::BulletsManager() : rotation(0.)
{
}

void BulletsManager::addBullet(const sf::Vector2f from, const sf::Vector2f to)
{
    if (canAddBullet())
    {
        BulletSprite *bullet = new BulletSprite();
        lastShoot.restart();
        bullets.push_back(bullet);
    }
}

bool BulletsManager::canAddBullet() const
{
    return lastShoot.getElapsedTime().asMilliseconds() > BeetweenTwoShootInMs;
}
