#include "character.hpp"
#include "../constants.hpp"

#include <cmath>

static const float rad2deg = 180. / M_PI;

CharacterSprite::CharacterSprite()
{
    texture.loadFromFile("data/character.png");
    setTexture(texture);

    sf::Vector2f position(ScreenWidth / 2, ScreenHeight / 2);
    setPosition(position);

    sf::Vector2f origin(getLocalBounds().width / 2, getLocalBounds().height / 2);
    setOrigin(origin);
}

void CharacterSprite::rotateFromMousePosition(const int x, const int y)
{
    float angle = getAngleFromMouse(x, y);
    setRotation(angle);
}

float CharacterSprite::getAngleFromMouse(const int pos_x, const int pos_y) const
{
    int x = pos_x - getPosition().x;
    int y = pos_y - getPosition().y;
    float hypo = sqrtf(x * x + y * y);
    float angle = atan2f(y, x) * rad2deg + 90;

    return angle;
}

void CharacterSprite::move(Movment movt)
{
    sf::Vector2f position = getPosition();

    switch (movt)
    {
    case Movment::Up:
        position.y -= 5;
        break;

    case Movment::Down:
        position.y += 5;
        break;

    case Movment::Right:
        position.x -= 5;
        break;

    case Movment::Left:
        position.x += 5;
        break;
    }
    
    setPosition(position);
}