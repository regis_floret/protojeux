#ifndef __CHARACTER_HPP__
#define __CHARACTER_HPP__

#include <SFML/Graphics.hpp>

class CharacterSprite : public sf::Sprite
{
  public:
    enum Movment {
        Up,
        Down,
        Left,
        Right
    };

    CharacterSprite();
    void rotateFromMousePosition(const int x, const int y);
    void move(Movment mov);

  private:
    float getAngleFromMouse(const int pos_x, const int pos_y) const;

  private:
    sf::Texture texture;
};

#endif