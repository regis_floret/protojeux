#include "game.hpp"

const int ScreenWidth = 1024;
const int ScreenHeight = 600;

Game::Game()
{
    window.create(sf::VideoMode(ScreenWidth, ScreenHeight), "Protoype Jeux", sf::Style::Titlebar | sf::Style::Close);
    window.setMouseCursorVisible(false);
    
    introPanel = new IntroPanel;
    playPanel = new PlayPanel;    
    
    currentPanel = introPanel;
}

Game::~Game()
{

}

void Game::run()
{

    while (window.isOpen())
    {
        sf::Event event;

        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed || event.type == sf::Event::KeyPressed && event .key.code == sf::Keyboard::Escape)
            {
                window.close();
                break;
            }

            IPanel::Action action = currentPanel->move(event);

            switch (action) 
            {
                case IPanel::Action::Quit:
                    window.close();
                    break;

                case IPanel::Action::Play:
                    currentPanel = playPanel;                
                    break;

                default:
                    // Do nothing
                    ;
            }
        }

        window.clear();
        currentPanel->render(window);
        window.display();
    }
}