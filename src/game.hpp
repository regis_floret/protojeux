#ifndef __GAME_H__
#define __GAME_H__

#include "constants.hpp"
#include "panels/ipanel.hpp"
#include "panels/intropanel.hpp"
#include "panels/playpanel.hpp"

#include <SFML/Graphics.hpp>

class Game 
{
public:
    Game();  
    ~Game();
    void run();

private:
    sf::RenderWindow window;
    IPanel * currentPanel;
    IntroPanel * introPanel;
    PlayPanel * playPanel;
};

#endif

